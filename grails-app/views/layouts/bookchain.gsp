<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
    <asset:stylesheet src="reen-theme.css"/>

		
		<g:layoutHead/>
		
	</head>

	<body>

		<!-- ============================================================= HEADER ============================================================= -->
		
		<header>
			<div class="navbar">
				
				<div class="yamm">
					<div class="navbar-collapse collapse">
						<div class="container">
							
							<!-- ============================================================= LOGO ============================================================= -->
							
							<a class="navbar-brand" href="/#">
		    					<asset:image src="bookchain.png" alt="Bookchain Logo"/>
                			</a>
							
							<!-- ============================================================= LOGO : END ============================================================= -->
							
							
							<!-- ============================================================= MAIN NAVIGATION ============================================================= -->
							
							<ul class="nav navbar-nav">
								
								<li class="dropdown">
									<a href="index3.html" class="dropdown-toggle" data-toggle="dropdown">Inicio</a>
								</li><!-- /.dropdown -->

								<li class="dropdown">
									<a href="about.html" class="dropdown-toggle" data-toggle="dropdown">Quienes Somos</a>
								</li><!-- /.dropdown -->

								<li class="dropdown">
									<a href="about.html" class="dropdown-toggle" data-toggle="dropdown">Bliblioteca</a>
											<ul class="dropdown-menu">
												<li><a href="portfolio.html">Apuntes</a></li>
												<li><a href="portfolio2.html">Examenes</a></li>
											</ul><!-- /.dropdown-menu -->

								</li><!-- /.dropdown -->

								<li class="dropdown">
									<a href="contact.html" class="dropdown-toggle" data-toggle="dropdown">Contacto</a>
								</li><!-- /.dropdown -->

								<li class="dropdown">
									<a href="contact.html" class="dropdown-toggle" data-toggle="dropdown">Regístro</a>
								</li><!-- /.dropdown -->
								
								<li class="dropdown pull-right searchbox">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-search"></i></a>
											  
									<div class="dropdown-menu">
										<form id="search" class="navbar-form search" role="search">
											<input type="search" class="form-control" placeholder="Type to search">
											<button type="submit" class="btn btn-default btn-submit icon-right-open"></button>
										</form>
									</div><!-- /.dropdown-menu -->
								</li><!-- /.searchbox -->
								
							</ul><!-- /.nav -->
							
							<!-- ============================================================= MAIN NAVIGATION : END ============================================================= -->
							
						</div><!-- /.container -->
					</div><!-- /.navbar-collapse -->
				</div><!-- /.yamm -->
			</div><!-- /.navbar -->
		</header>
		
		<!-- ============================================================= HEADER : END ============================================================= -->
		
		<g:layoutBody/>

		<!-- ============================================================= FOOTER ============================================================= -->
		
		<footer class="dark-bg">
			<div class="container inner">
				<div class="row">
					
					<div class="col-md-6 col-sm-6 inner">
						<h4>Quienes Somos</h4>
						<a href="index.html">
							<asset:image src="bookchain_white.png" alt="Bookchain Logo" class="logo img-intext"/>
						</a>
						<p>Como estudiantes buscamos solucionar problematicas a las que comunmente nos afrontamos en nuestra vida diaria.</p>
						<a href="about.html" class="txt-btn">Más acerca de nosotros</a>
					</div><!-- /.col -->

					
					<div class="col-md-6 col-sm-6 inner">
						<h4>Contáctanos</h4>
						<p>Si tienes alguna duda con respecto a nuestros servicios no dudes en contactarnos</p>
						<ul class="contacts">
							<li><i class="icon-location contact"></i>CDMX</li>
							<li><i class="icon-mobile contact"></i> +52 1 55 51 84 19 44</li>
							<li><a href="#">
								<i class="icon-mail-1 contact">
								</i> contact@bookchain.com
								</a>
							</li>
						</ul><!-- /.contacts -->
					</div><!-- /.col -->
					
			
					
				</div><!-- /.row --> 
			</div><!-- .container -->
		  
			<div class="footer-bottom">
				<div class="container inner">
					<p class="pull-left">© 2017 Book-Chain. All rights reserved.</p>
				</div><!-- .container -->
			</div><!-- .footer-bottom -->
		</footer>
		
		<!-- ============================================================= FOOTER : END ============================================================= -->
    	<asset:javascript src="application.js"/>

	</body>
</html>