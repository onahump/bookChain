<html>
<html>
<head>
    <meta name="layout" content="bookchain"/>
    <title>Home Page</title>
</head>
<body>

<!-- ============================================================= MAIN ============================================================= -->				
		
		<main>
			
			
			<!-- ============================================================= SECTION – HERO ============================================================= -->
			
			<section id="hero">
				<div id="owl-main" class="owl-carousel owl-one-item">
					
					<div class="item img-bg-center" style="background-image: url(assets/images/art/slider02.jpg);">
						<div class="container">
							<div class="caption vertical-center text-center">
								
								<h1 class="fadeInDown-1 light-color">Made for Designers</h1>
								<p class="fadeInDown-2 light-color">Create your online portfolio in minutes with the professionally and lovingly designed REEN website template. Customize your site with versatile and easy to use features.</p>
								<div class="fadeInDown-3">
									<a href="#" class="btn btn-large">Get started now</a>
								</div><!-- /.fadeIn -->
								
							</div><!-- /.caption -->
						</div><!-- /.container -->
					</div><!-- /.item -->
					
				</div><!-- /.owl-carousel -->
			</section>
			
		</main>
		
		<!-- ============================================================= MAIN : END ============================================================= -->
	

</body>
</html>