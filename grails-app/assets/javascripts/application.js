// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require third-party/jquery/dist/jquery.js
//= require third-party/bootstrap/dist/js/bootstrap.js
//= require third-party/jquery/dist/jquery.minjs
//= require third-party/jquery.easing/js/jquery.easing.1.3.min
//= require third-party/jquery.easing/js/jquery.form.js
//= require third-party/jquery.easing/js/jquery.validate.min.js
//= require third-party/bootstrap/dist/js/bootstrap.min.js
//= require third-party/aos/dist/aos.js
//= require third-party/owl.carousel/dist/owl.carousel.min.js
//= require third-party/owl.carousel/dist/owl.carousel.JavaScript
//= require third-party/jquery.easing/js/jquery.isotope.min.js
//= require third-party/imagesloaded/imagesloaded.pkgd.min.js
//= require third-party/jquery.easing/js/jquery.easytabs.min.js
//= require third-party/viewport-units-buggyfill/viewport-units-buggyfill.js
//= require third-party/reen-theme/assets/js/scripts.js
//= require third-party/reen-theme/assets/js/custom.js  

















if (typeof jQuery !== 'undefined') {
    (function($) {
        $(document).ajaxStart(function() {
            $('#spinner').fadeIn();
        }).ajaxStop(function() {
            $('#spinner').fadeOut();
        });
    })(jQuery);
}
